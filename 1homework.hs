import CodeWorld

-- Creates pictures of traffic light for specific time
--   It draws "on" states over lamp - traffic light with all "off" states
trafficSwitch :: Double -- Time
  -> Picture
trafficSwitch t
    | i `mod` 8 < 3 = (greenLamp !! 0) <> lamp
    | i `mod` 8 == 3 = (yellowLamp !! 0) <> lamp
    | i `mod` 8 < 7 = (redLamp !! 0) <> lamp
    | i `mod` 8 == 7 = (yellowLamp !! 0) <> (redLamp !! 0)  <> lamp
    where i = round t


-- The traffic light with all lamps in state "off"
lamp = (greenLamp !! 1) <> (yellowLamp !! 1) <> (redLamp !! 1) <> frame

frame = rectangle 2.5 7

greenLamp = trafficLamp green 2
yellowLamp = trafficLamp yellow 0
redLamp = trafficLamp red(-2)


-- Creates a list of two pictures, 1-st picture is the "on" state, 2-nd - "off" state 
trafficLamp :: Color -- Color of "on" state
  -> Double -- y coordinate
  -> [Picture]
trafficLamp color y = [lightCircle color y, lightCircle darkerColor y]
    where 
    lightCircle color y =  translated 0 y (colored color (solidCircle 1))
    darkerColor = darker 0.4 color -- The color of "off" state 

----------------------------------------------------------------------

-- Fractal tree at particular moment 't' of growth
tree :: Double -> Picture
tree t
  | t < 1 = colored red (branch highestBranchLen 0 0.1 <> fruit) -- the highest branch
  | t > 1 =  mainBranch <> translated 0 1 (leftBranch <> rightBranch)
  where
  fruit = translated 0 highestBranchLen (circle 0.1) -- is placed on the highest branch
  tFloorPlusOne = fromIntegral (floor t) + 1
  highestBranchLen = t/tFloorPlusOne

  lowestWidth = (t-1)*0.1 
  highestWidth = t*0.1 

  mainBranch = branch 1 lowestWidth highestWidth
  leftBranch = rotated (pi/8) (tree (t - 1) )
  rightBranch = rotated (-pi/8) (tree (t - 1))

-- The branch of tree (trapezoid when thin /= 0; triangle if thin = 0)
branch :: Double -- length of pranch
  -> Double -- 1/2 width of the smallest base of the trapezoid
  -> Double -- 1/2 width of the biggest base of the trapezoid
  -> Picture
branch len thin large = solidPolygon [(thin, len), (-thin, len), (-large, 0), (large, 0)]

solution2 = animationOf tree -- fractal tree with thikness and leaves
solution1 = trafficSwitch -- complex traffic lights
main :: IO ()
main = animationOf trafficSwitch
