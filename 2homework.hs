{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}
import CodeWorld
import Data.Ix

type Coords = (Int, Int)

data Dir = U | D | L | R deriving Eq

-- Creates squares with custom color, but fixed dimension
coloredSquare :: Color -> Picture
coloredSquare c = colored c (solidRectangle 1 1)

coloredCircle :: Color -> Picture
coloredCircle c = colored c (solidCircle 0.4)

type DoorColor = Color
data Tile = Exit | Floor | Button DoorColor | Door DoorColor | Wall | Void deriving Eq

-- Creates exit tile picture by inserting picture of black and red squares inside itself
exitTile :: Picture
exitTile = fractalSquare numberOfInnerSquares
  where 
    numberOfInnerSquares = 3
    coef = 0.8 -- ratio between outer and inner squares
    blackSquare = coloredSquare black
    redSquare = scaled coef coef (coloredSquare red)
    base = redSquare <> blackSquare
    fractalSquare :: Int -> Picture
    fractalSquare i
      | i == 0 = base
      | otherwise = scaled (coef^i) (coef^i) (fractalSquare (i - 1)) <> base

 
type Circle = (Double, Double, Double)


-- Determines whether the given point approximately lies on the given circle or not
onCircle :: Circle 
  -> Coords -- point
  -> Bool
onCircle (r, x, y) (i, j) = abs (distanceFromTo (x, y) (di, dj) - r) < 0.5
  where
  di = fromIntegral i
  dj = fromIntegral j 
  
  distanceFromTo :: (Double, Double) -> (Double, Double) -> Double
  distanceFromTo (x, y) (i, j) = sqrt ((x - i)^2 + (y-j)^2) 
 
 
-- Determines whether the given point lies on one of the given circles
--   it is possible to define several circles by passing their number, 
--   and ratio between the radiuses of consequent circles
onCircles :: Coords -> Coords -> Double -> Double -> Bool
onCircles (x, y) (cx, cy) num factor
  | num == 0 = False
  | isOnCircle = True
  | otherwise = onCircles (x, y) (cx, cy) (num - 1) factor
  where
  radius = num * factor
  dcx = fromIntegral cx
  dcy = fromIntegral cy
  isOnCircle = onCircle (radius, dcx, dcy) (x, y) 

-- Maps coordinates to tiles
myLevelMap :: Coords -> Tile
myLevelMap (x, y)
  | x > 21 || x < 0 = Void
  | y > 21 || y < 0 = Void
  | x < 19 && x > 2 && y == 0 = Door blue
  | x < 19 && x > 2 && y == 1 = Door red
  | onCircles (x, y) (0, 0) 3 3 = Wall
  | onCircles (x, y) (5, 15) 3 3 = Wall
  | onCircles (x, y) (21, 21) 3 3 = Door blue
  | onCircles (x, y) (21, 0) 4 3 = Wall
  | onCircles (x, y) (21, 0) 5 2 = Button red
  | onCircles (x, y) (0, 0) 4 2 = Button blue
  | onCircles (x, y) (21, 21) 4 2 = Button blue
  | (x, y) == (21, 21) = Exit
  | otherwise = Floor

-- Maps direction to the coordinates changed by following given direction
coordsInDir :: Dir -> Coords -> Coords
coordsInDir U (fx, fy) = (fx, fy + 1)
coordsInDir D (fx, fy) = (fx, fy - 1)
coordsInDir L (fx, fy) = (fx - 1, fy)
coordsInDir R (fx, fy) = (fx + 1, fy)


-- Allows to acces color of tile though some tiles don't have colors
getTileColor :: Tile -> Maybe DoorColor
getTileColor (Button col) = Just col
getTileColor (Door col) = Just col
getTileColor _ = Nothing
 
-- Changes coords according to the given direction 
tryMove :: Dir -> State -> State
tryMove dir (State (fromX, fromY) doors)
  | canMove toTile = State toTileCoords updatedDoors
  | otherwise = State (fromX, fromY) updatedDoors
  where
  updatedDoors = updateDoors doors
  toTileCoords = coordsInDir dir (fromX, fromY)
  toTile = updatedMap (State (fromX, fromY) doors) toTileCoords
  
  canMove :: Tile -> Bool
  canMove toTile 
    | toTile == Void = False
    | toTile == Wall = False
    | toTile == (Door blue) = False
    | toTile == (Door red) = False
    | otherwise = True
  
  tileMaybeColor = getTileColor toTile
  tileColor = maybe gray id tileMaybeColor
  
  updateDoors :: [DoorColor] -> [DoorColor]
  updateDoors initialDoors 
    | tileMaybeColor == Nothing = initialDoors
    | not (oneOf tileColor initialDoors) && isButton toTile = tileColor:initialDoors
    | otherwise = initialDoors
  
  isButton (Button _) = True
  isButton _ = False
  
  
-- Defines pictures for all variations of tiles 
drawTile :: Tile -> Picture
drawTile Void = blank
drawTile Wall = coloredSquare black
drawTile Exit = exitTile
drawTile Floor = coloredSquare brown
drawTile (Button color) = coloredCircle color <> drawTile Floor
drawTile (Door color) = coloredCircle color <> drawTile Wall


-- Is used to generate pictures of tiles given coords and map
drawTileAt :: Coords -> (Coords -> Tile) -> Picture 
drawTileAt (x, y) myMap = drawTile (myMap (x, y))


-- Is used draw something in range with given render
drawFromTo :: Coords -> (Int -> Picture) -> Picture
drawFromTo (from, to) render
  | from > to = blank
  | otherwise = render from <> drawFromTo (from + 1, to) render
  
-- Is used to draw rows of tiles in range of specified columns; 
--   places tiles on right places
drawRowFromTo :: Int -- Number of rows
  -> Coords -- Range of columns
  -> (Coords -> Tile) -- Map
  -> Picture
drawRowFromTo y (from, to) myMap = drawFromTo (from, to) drawRowTile
  where
  dy = fromIntegral y
  drawRowTile x = translated (fromIntegral x) dy (drawTileAt (x, y) myMap)


drawMap :: (Coords -> Tile) -> Picture
drawMap myMap = drawFromTo (0, size) drawRow
  where
  drawRow y = drawRowFromTo y (0, size) myMap
  size = 21

-- Creates map with open doors
openDoors :: [DoorColor] -- Doors to open.
  -> (Coords -> Tile) -- Original map.
  -> (Coords -> Tile) -- Map with doors open.
openDoors toOpen initialMap (x, y)
  | maybeColor == Nothing  = initialMap (x, y)
  | oneOf color toOpen  = Floor
  | otherwise = initialMap (x, y)
  where 
  maybeColor = getTileColor (initialMap (x, y))
  color = maybe gray id maybeColor

-- True if color belongs to the list of colors
oneOf :: DoorColor -> [DoorColor] -> Bool
oneOf color colors
  | colors == [] = False
  | color == head colors = True
  | otherwise = oneOf color (tail colors)


data State = State Coords [DoorColor]

handleWorld :: Event -> State -> State
handleWorld (TimePassing _) = id
handleWorld (KeyPress "Up") = tryMove U
handleWorld (KeyPress "Down") = tryMove D
handleWorld (KeyPress "Left")  = tryMove L
handleWorld (KeyPress "Right") = tryMove R
handleWorld (_) = id

data Player = Player Picture Coords

player :: Player
player = Player picture (0, 0)
  where picture = lettering "🚗"

-- Replaces the picture of player
movePlayer :: Player -> Coords -> Picture
movePlayer (Player picture from) to = translated dx dy picture
  where
  (fx, fy) = from
  (tx, ty) = to
  dx = fromIntegral (tx - fx)
  dy = fromIntegral (ty - fy)

-- Map with open doors
updatedMap ::State -> Coords -> Tile
updatedMap (State _ openedColors) = openDoors openedColors myLevelMap

renderWorld :: State -> Picture
renderWorld (State coords openedColors) = scaled 0.8 0.8 (translated (-10) (-10) worldPicture)
  where
  playerPicture = movePlayer player coords
  mapPicture = drawMap (updatedMap (State coords openedColors))
  worldPicture = playerPicture <> mapPicture


initialWorld :: State 
initialWorld = State (0, 0) []

solution1 = activityOf initialWorld handleWorld renderWorld
solution2 = drawingOf (drawMap myLevelMap)

solution3 = drawingOf (drawMap allDoorsOpen)
  where 
  allDoorsOpen = openDoors [red, blue] myLevelMap

solution4 = activityOf initialWorld handleWorld renderWorld

main :: IO ()
main = solution4
